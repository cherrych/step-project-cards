# Step Project Cards

## Developers

- Artur Cherychenko
- Sergey Kabanets

# Technologies used in this project:
- Webpack
- Bootstrap 5
- AJAX AXIOS
- ES6 class
- JS, SCSS, CSS


# Task.

- Artur Cherychenko:


  Creating class, modal-window and functionality of the visit creation card.<br>
  Creation of the main class of visits and child visits of doctors.<br>
  Creating requests for create and delete visits.<br>
  Creating and functionality delete cards of visits.<br>
  Creating styles for cards of visits.


- Sergey Kabanets:


  Creating layout application and base styles for elements.<br>
  Creating a project structure.<br>
  Creating classes ModalAuthorization, ModalEditVisit.<br>
  Creating requests for application login and edit visits.<br>
  Creating modals windows for authorization, edit and change status of visits.<br>
  Creating and functionality filters for cards of visits.
