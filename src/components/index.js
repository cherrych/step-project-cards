import { setMainPage } from "./functions/Main-page/setMainPage.js";
import { authorization } from "./functions/authorization.js";
import { createVisit } from "./functions/createVisit.js"
import { logout } from "./functions/logout.js";

setMainPage();
authorization();
createVisit();
logout();