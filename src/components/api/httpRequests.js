import { request } from "./request.js";

export const getToken = async ({ email, password}) => {
    const { res, err } = await request({ url: '/login', method: 'POST', body: { email, password } });

    if (!!res) {
        window.localStorage.setItem('token', res);
    } else {
        throw new Error(`There is no such user: ${err.data}`);
    }
};

export const getAllCards = async () => {
    const { res, err } = await request();

    if (!!res) {
        localStorage.setItem('allVisitCards', JSON.stringify(res));
        return res;
    } else {
        throw new Error(`Failed to receive cards: ${err.data}`);
    }
};

export const createCard = async visitDetails => {
    const { res, err } = await request({ method: 'POST', body: visitDetails });

    if (!!res) {
        return res;
    } else {
        throw new Error(`Failed to create card:  ${err.data}`);
    }
};


export const editCard = async ({ visitId, visitDetails }) => {
    const { res, err } = await request({ method: 'PUT', url: `/${visitId}`, body: visitDetails });

    if (!!res) {
        return res;
    } else {
        throw new Error(`Couldn't update card: ${err.data}`);
    }
};

export const deleteCardRequest = async cardId => {
    const { status, err } = await request({ method: 'DELETE', url: `/${cardId}` });

    if (!!status) {
        return status;
    } else {
        throw new Error(`Couldn't delete card: ${err.data}`);
    }
};