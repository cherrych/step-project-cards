import { visitCardsStorage } from "../variables/variables.js";
import { ModalEditVisit } from "../classes/Modal/ModalEditVisit.js";
import { formToObject } from "./formToObject.js";
import { editCard} from "../api/httpRequests.js";
import { getAllVisitCards } from "./getAllVisitCards.js";


export const editVisitCard = () => {

    const editButtons = visitCardsStorage.querySelectorAll('.edit-visit-btn');

    editButtons.forEach( edit => {
        edit.addEventListener('click', () => {
            let visitId = Number(edit.getAttribute('data-card'));

            let arrayVisits = JSON.parse(localStorage.getItem('allVisitCards'));
            let visitCard;
            arrayVisits.forEach( card => {
                if(visitId === card.id){
                    visitCard = {...card};
                }
            })

            const modalEditVisit = new ModalEditVisit(visitCard);
            modalEditVisit.render();

            const form = document.querySelector(".form");
            const createBtn = form.querySelector(".submit");
            createBtn.addEventListener("click", async (e) => {
                e.preventDefault();

                const formData = new FormData(form);
                const visitDetails = formToObject(formData);
                visitDetails.status = 'Open';
                modalEditVisit.close();
                console.log(visitDetails);

                await editCard({visitId, visitDetails});
                await getAllVisitCards();
            })
        })
    })

    const statusButtons = visitCardsStorage.querySelectorAll('.btn-status');

    statusButtons.forEach( status => {
        status.addEventListener('click', async () => {

            let visitId = Number(status.getAttribute('data-card'));

            let arrayVisits = JSON.parse(localStorage.getItem('allVisitCards'));
            let visitCard;

            arrayVisits.forEach( card => {
                if (visitId === card.id){
                    visitCard = {...card};
                }
            })

            const visitDetails = visitCard;

            const statusAttribute = status.getAttribute('data-status');

            if (statusAttribute === 'Open') {
                visitDetails.status = 'Done';
            } else {
                visitDetails.status = 'Open';
            }

            await editCard({visitId, visitDetails});
            await getAllVisitCards();
        })
    })

}