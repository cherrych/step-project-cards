import {filter, filterPriority, filterSearch, filterStatus} from "../../variables/variables.js";
import { renderVisitCards } from "../renderVisitCards.js";

export const setFilterSearch = (arrayVisitCards) => {

    filterSearch.addEventListener('input', e => {

        if( filterStatus.value !== 'all' ||  filterPriority.value !== "all") {
            filter.reset();
        }

        const value = e.target.value;

        if (value === '') {
            renderVisitCards(arrayVisitCards);
        } else {
            let filteredArraySearch;
            filteredArraySearch = arrayVisitCards.filter(visit =>
                visit.fullName && visit.fullName.toLowerCase().includes(value.toLowerCase())
            );
            renderVisitCards(filteredArraySearch);
        }
    })
}

