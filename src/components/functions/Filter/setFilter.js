import { setFilterStatus } from "./setFilterStatus.js";
import { setFilterPriority } from "./setFilterPriority.js";
import { setFilterSearch } from "./setFilterSearch.js";
import { filter } from "../../variables/variables.js";

export const setFilter = (allVisitCards) => {

    filter.addEventListener('submit', e => {
        e.preventDefault();
    })

    setFilterStatus(allVisitCards);
    setFilterPriority(allVisitCards);
    setFilterSearch(allVisitCards);
};
