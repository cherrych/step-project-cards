import { visitCardsStorage } from "../variables/variables.js";
import { VisitCardiologist } from "../classes/Visit/VisitCardiologist.js";
import { VisitDentist } from "../classes/Visit/VisitDentist.js";
import { VisitTherapist } from "../classes/Visit/VisitTherapist.js";
import { editVisitCard } from "./editVisitCard.js";

export const renderVisitCards = (arrayVisitCards) => {
    visitCardsStorage.innerHTML = '';

    if(arrayVisitCards.length === 0){
        visitCardsStorage.insertAdjacentHTML('beforeend', `
         <p class="card-storage__text-empty-storage">No items have been added</p>`)
    } else {
        arrayVisitCards.forEach( visit => {
            if(visit.selectDoctor === "Cardiologist") {
                const visitCard = new VisitCardiologist(visit);
                visitCard.render(visitCardsStorage);
            } else if (visit.selectDoctor === "Dentist") {
                const visitCard = new VisitDentist(visit);
                visitCard.render(visitCardsStorage);
            } else if (visit.selectDoctor === "Therapist") {
                const visitCard = new VisitTherapist(visit);
                visitCard.render(visitCardsStorage);
            }
        })

        editVisitCard();
    }

}