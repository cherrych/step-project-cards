import {deleteCardRequest} from "../api/httpRequests";
import {getAllVisitCards} from "./getAllVisitCards";


export const deleteCard = async (cardId) => {
await deleteCardRequest(cardId)
    await getAllVisitCards();
};