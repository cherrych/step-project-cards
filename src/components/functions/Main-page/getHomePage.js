import { loginButton, createVisitButton, logoutButton, filterWrapper } from "../../variables/variables.js";

export const getHomePage = () => {
    loginButton.classList.add('hide');
    logoutButton.classList.remove('hide');
    createVisitButton.classList.remove('hide');
    filterWrapper.classList.remove('hide');
}