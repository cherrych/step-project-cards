import {Modal} from "./Modal.js";

export class ModalCreateVisit extends Modal {
    constructor(title = "Create new visit".toUpperCase(),) {
        super(title);
        this.body = `
      <form class="form g-2 mt-0 justify-content-evenly was-validated">
        <div class="pt-3">
          <label class="label">Full name</label>
          <input
            type="text"
            name="fullName"
            class="input form-control mb-2"
            placeholder="Enter full name"
            required
          />
          <label class="label">Visit purpose</label>
          <input
            type="text"
            name="visitPurpose"
            class="input form-control mb-2"
            placeholder="Enter visit purpose"
            required
          />
          <label class="label">Priority</label>
          <select name="priority" class="form-select mb-2 priority" required>
            <option selected disabled value="">Choose...</option>
            <option value="High">High</option>
            <option value="Normal">Normal</option>
            <option value="Low">Low</option>
          </select>
        </div>
        <div class="select-doctor">
          <label class="label">Doctor's choice</label>
          <select name="selectDoctor" class="select-doctor form-select mb-2" required>
            <option selected disabled value="">Choose...</option>
            <option value="Cardiologist">Cardiologist</option>
            <option value="Dentist">Dentist</option>
            <option value="Therapist">Therapist</option>
          </select>
        </div>
        <div id="additional"></div>
        <label class="label">Short description</label>
        <textarea class="form-control" name="description" rows="2"></textarea>         
        <div class="wrap_btn">
          <button 
            type="button"
            class="btn btn-primary px-5 py-2 close"
            data-bs-dismiss="modal"
          >
            Close
          </button>
          <button type="submit" class="btn btn-primary px-5 py-2 submit">Create</button>
        </div>
      </form>`;
    }

    render() {
        super.render();

        const form = document.querySelector(".form");
        const additionalRow = form.querySelector("#additional");
        const selectDoctors = form.querySelector(".select-doctor");

        selectDoctors.addEventListener("change", (e) => {
            const value = e.target.value;
            switch (value) {
                case "Cardiologist":
                    additionalRow.innerHTML = "";
                    additionalRow.insertAdjacentHTML("beforeend", `<div class="cardiologist pt-4">
            <label class="label">Normal pressure</label>
            <input
              type="text"
              name="pressure"
              class="input form-control mb-2"
              placeholder="120/80"
              required
            />
            <label class="label">Body mass index</label>
            <input
              type="text"
              name="bodyMassIndex"
              class="input form-control mb-2"
              placeholder="I=m/h*h"
              required
            />
            <label class="label">Age</label>
            <input
              type="number"
              class="input form-control mb-2"
              name="age"
              placeholder="Enter age"
              min="1"
              max="110"
              required
            />
            <label class="label">Past diseases of the CVS</label>
            <input type="text" name="CVS" class="input form-control mb-2" required />
          </div>`);
                    break;
                case "Dentist":
                    additionalRow.innerHTML = "";
                    additionalRow.insertAdjacentHTML("beforeend", `<div class="dentist pt-4">
            <label class="label">Date of last visit</label>
            <input type="date" name="lastVisit" class="input form-control mb-2" required />
          </div>`);
                    break;
                case "Therapist":
                    additionalRow.innerHTML = "";
                    additionalRow.insertAdjacentHTML("beforeend", `<div class="therapist pt-4">
            <label class="label">Age</label>
            <input
                type="number"
                name="age"
                class="input form-control mb-2"
                min="1"
                max="110"
                required
            />
          </div>`);
                    break;
            }
        });
    }
}