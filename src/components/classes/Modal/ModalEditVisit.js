import { Modal } from "./Modal.js";

export class ModalEditVisit extends Modal{
    constructor ({id, fullName, visitPurpose, priority, selectDoctor, description, status,  pressure = '', bodyMassIndex = '', CVS = '', age = '', lastVisit = ''}, title = 'Edit visit'.toUpperCase()) {
        super(title);
        this.id = id;
        this.fullName = fullName;
        this.visitPurpose = visitPurpose;
        this.priority = priority;
        this.selectDoctor = selectDoctor;
        this.description = description;
        this.status = status;
        this.pressure = pressure;
        this.bodyMassIndex = bodyMassIndex;
        this.CVS = CVS;
        this.age = age;
        this.lastVisit = lastVisit;
        this.body = `
            <form class="form g-2 mt-0 justify-content-evenly was-validated">
                <div class="pt-3">
                    <label class="label">Full name</label>
                    <input type="text" value="${this.fullName}" name="fullName" class="input form-control mb-2" placeholder="Enter full name" required />
                    <label class="label">Visit purpose</label>
                    <input type="text" value="${this.visitPurpose}" name="visitPurpose" class="input form-control mb-2" placeholder="Enter visit purpose" required />
                    <label class="label">Priority</label>
                    <select name="priority" class="form-select mb-2 priority" id="priority" required>
                        <option selected disabled value="">Choose...</option>
                        <option value="High">High</option>
                        <option value="Normal">Normal</option>
                        <option value="Low">Low</option>
                    </select>
                </div>
                <div class="select-doctor">
                    <label class="label">Doctor's choice</label>
                    <select name="selectDoctor" class="select-doctor form-select mb-2" id="selectDoctor" required>
                        <option selected disabled value="">Choose...</option>
                        <option value="Cardiologist">Cardiologist</option>
                        <option value="Dentist">Dentist</option>
                        <option value="Therapist">Therapist</option>
                    </select>
                </div>
                <div id="additional"></div>
                <label class="label">Short description</label>
                <textarea class="form-control" name="description" rows="2">${this.description}</textarea>
                <div class="wrap_btn">
                    <button type="button" class="btn btn-primary px-5 py-2 close" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary px-5 py-2 submit">Edit Visit</button>
                </div>
           </form>`;
    }

    render() {
        super.render();

        const form = document.querySelector(".form");
        const additionalRow = form.querySelector("#additional");
        const selectDoctors = form.querySelector(".select-doctor");

        selectDoctors.addEventListener("change", (e) => {
            const value = e.target.value;
            switch (value) {
                case "Cardiologist":
                    additionalRow.innerHTML = "";
                    additionalRow.insertAdjacentHTML("beforeend", `
                        <div class="cardiologist pt-4">
                            <label class="label">Normal pressure</label>
                            <input type="text" name="pressure" value="${this.pressure}" class="input form-control mb-2" placeholder="120/80" required />
                            <label class="label">Body mass index</label>
                            <input type="text" name="bodyMassIndex" value="${this.bodyMassIndex}" class="input form-control mb-2" placeholder="I=m/h*h" required />
                            <label class="label">Age</label>
                            <input type="number" value="${this.age}" class="input form-control mb-2" name="age" placeholder="Enter age" min="1" max="110" required /> 
                            <label class="label">Past diseases of the CVS</label>
                            <input type="text" name="CVS" value="${this.CVS}" class="input form-control mb-2" required />
                        </div>`);
                    break;
                case "Dentist":
                    additionalRow.innerHTML = "";
                    additionalRow.insertAdjacentHTML("beforeend", `
                        <div class="dentist pt-4">
                            <label class="label">Date of last visit</label>
                            <input type="date" name="lastVisit" value="${this.lastVisit}" class="input form-control mb-2" required />
                        </div>`);
                    break;
                case "Therapist":
                    additionalRow.innerHTML = "";
                    additionalRow.insertAdjacentHTML("beforeend", `
                        <div class="therapist pt-4">
                        <label class="label">Age</label>
                        <input type="number" name="age" value="${this.age}" class="input form-control mb-2" min="1" max="110" required /> </div>`);
                    break;
            }
        });

        let doctorSelector = form.querySelector('#selectDoctor');
        doctorSelector.value = this.selectDoctor;
        doctorSelector.dispatchEvent(new Event('change', {bubbles: true}));
        form.querySelector('#priority').value = this.priority;

    }
}