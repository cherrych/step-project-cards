import { Modal } from "./Modal.js";

export class ModalLogin extends Modal {
    constructor(title = 'User Authorization'.toUpperCase()) {
        super(title);
        this.body = `
        <form id="form-login">
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Email address</label>
                <input type="email" class="form-control" id="user-email" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
                 <label for="exampleInputPassword1" class="form-label">Password</label>
                 <input type="password" class="form-control" id="user-password">
            </div>
            <p class="authorization-error-massage"></p>
            <button id="authorization" type="submit" class="authorization-button btn btn-primary">Login</button>
        </form>
        `
    }
}