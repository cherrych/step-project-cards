import * as bootstrap from 'bootstrap';

export class Modal {
  constructor(title, body) {
    this.title = title;
    this.body = body;
  }

  render() {
    document.body.insertAdjacentHTML(
      "beforeend",
      `
        <div class="modal" tabindex="-1" id="main-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">${this.title}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>                  
                    <div class="modal-body">
                        ${this.body}
                    </div>
                </div>
            </div>
        </div>
        `
    );

    this.modal = new bootstrap.Modal('#main-modal', {
      keyboard: false
        });

    this.modal.show();

    this.modal._element.addEventListener("hidden.bs.modal", (event) =>
      event.target.remove()
    );
  }

  close() {
    this.modal.hide();
  }
}
