import { Visit } from "./Visit";

export class VisitCardiologist extends Visit {
    constructor({fullName, visitPurpose, priority, selectDoctor, description, status, id, pressure, bodyMassIndex, CVS, age}) {
        super({fullName, visitPurpose, priority, selectDoctor, description, status, id});
        this.pressure = pressure;
        this.bodyMassIndex = bodyMassIndex;
        this.CVS = CVS;
        this.age = age;
    }
    render(parent) {
        super.render(parent);

        this.addRow = this.card.querySelector('#additional-row');
        this.addRow.insertAdjacentHTML("beforeend", `
        <li class="card-list-item list-group-item">Normal pressure: ${this.pressure}</li>
        <li class="card-list-item list-group-item">Body mass index: ${this.bodyMassIndex}</li>
        <li class="card-list-item list-group-item">Cardiovascular diseases: ${this.CVS}</li>
        <li class="card-list-item list-group-item">Age: ${this.age}</li>
        `);
        this.card.classList.add("card-cardiologist");
        parent.append(this.card);
    }

}