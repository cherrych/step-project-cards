import { Visit } from "./Visit.js";

export class VisitDentist extends Visit {
    constructor({fullName, visitPurpose, priority, selectDoctor, description, status, id, lastVisit}) {
        super({fullName, visitPurpose, priority, selectDoctor, description, status, id});
        this.lastVisit = lastVisit;
    }
    render(parent) {
        super.render(parent);

        this.addRow = this.card.querySelector("#additional-row");
        this.addRow.insertAdjacentHTML("beforeend", `Date of last visit: ${this.lastVisit}`);
        this.card.classList.add("card-dentist");

        parent.append(this.card);
    }
}